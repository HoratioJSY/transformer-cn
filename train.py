# -*- coding: utf-8 -*-
#/usr/bin/python3

import tensorflow as tf

from model import Transformer
from tqdm import tqdm
from data_load import GetData
from prepro import PrePro
from utils import save_variable_specs, get_hypotheses, calc_bleu
import os
import math
import logging


class Config(object):
    # prepro
    en_vocab_size = 10000
    cn_vocab_size = 10000

    # preprocessed data path
    project_path = '/content/drive/My Drive/CN_NMT/'
    prepro_path = project_path + 'data/segmented/'
    data_path = project_path + 'data/'

    # training scheme
    batch_size = 32
    eval_batch_size = 32
    lr = 4e-4
    warmup_steps = 4000
    num_epochs = 10
    logdir = project_path + "/log/"
    eval_dir = project_path + "/eval/"
    bleu_ref = data_path + 'prepro/filtered_valid.cn'
    print_every = 1000

    # model setting
    d_model = 512
    d_feedforward = 1024
    num_blocks = 4
    num_heads = 8
    maxlen_en = 100
    maxlen_cn = 100
    dropout_rate = 0.3
    # label smoothing rate
    smoothing = 0.1

    # test setting
    ckpt_dir = project_path + '/checkpoint/'
    test_batch_size = 32
    testdir = './test/'


logging.basicConfig(level=logging.INFO)


def train_model(config):
    get_train = GetData(config)
    logging.info("# Prepare train/eval batches")
    train_batches, num_train_batches, num_train_samples = get_train.get_batch(config.batch_size,
                                                                              shuffle=True,
                                                                              mode='train')
    eval_batches, num_eval_batches, num_eval_samples = get_train.get_batch(config.batch_size,
                                                                           shuffle=False,
                                                                           mode='valid')

    # create a iterator of the correct shape and type
    iter = tf.data.Iterator.from_structure(train_batches.output_types, train_batches.output_shapes)
    xs, ys = iter.get_next()

    train_init_op = iter.make_initializer(train_batches)
    eval_init_op = iter.make_initializer(eval_batches)

    logging.info("# Load model")
    model = Transformer(config)
    loss, train_op, global_step, train_summaries = model.train(xs, ys)
    y_hat, eval_summaries = model.eval(xs, ys)

    if not os.path.exists(config.ckpt_dir): os.makedirs(config.ckpt_dir)
    if not os.path.exists(config.logdir): os.makedirs(config.logdir)
    if not os.path.exists(config.eval_dir): os.makedirs(config.eval_dir)

    logging.info("# Session")
    saver = tf.train.Saver(max_to_keep=config.num_epochs)
    with tf.Session() as sess:
        ckpt = tf.train.latest_checkpoint(config.ckpt_dir)
        if ckpt is None:
            logging.info("Initializing from scratch")
            sess.run(tf.global_variables_initializer())
            save_variable_specs(os.path.join(config.ckpt_dir, "specs"))
        else:
            saver.restore(sess, ckpt)

        summary_writer = tf.summary.FileWriter(config.logdir, sess.graph)

        sess.run(train_init_op)
        total_steps = config.num_epochs * num_train_batches
        _gs = sess.run(global_step)

        for i in tqdm(range(_gs, total_steps+1)):
            _, _gs, _summary = sess.run([train_op, global_step, train_summaries])
            epoch = math.ceil(_gs / num_train_batches)
            summary_writer.add_summary(_summary, _gs)
            
            if i > 0 and i % config.print_every == 0:
                _loss = sess.run(loss)
                print('training loss is %.4f' % _loss)

            if _gs and _gs % num_train_batches == 0:
                logging.info("epoch {} is done".format(epoch))
                _loss = sess.run(loss)
                print('after one epoch, loss is %.4f' % _loss)

                # logging.info("# test evaluation")
                # _, _eval_summaries = sess.run([eval_init_op, eval_summaries])
                # summary_writer.add_summary(_eval_summaries, _gs)

                # logging.info("# get hypotheses")
                # # model.idx2token is chinese
                # hypotheses = get_hypotheses(num_eval_batches, num_eval_samples, sess, y_hat, model.cn_idx2token)

                # logging.info("# write results")
                # model_output = "en2cn_E%02dL%.2f" % (epoch, _loss)
                # translation_path = os.path.join(config.eval_dir, model_output)
                # with open(translation_path, 'w') as fout:
                #     fout.write("\n".join(hypotheses))

                # # logging.info("# calc bleu score and append it to translation")
                # # calc_bleu(config.bleu_ref, translation_path)

                logging.info("# save models")
                ckpt_name = os.path.join(config.ckpt_dir, 'transformer')
                saver.save(sess, ckpt_name, global_step=_gs)
                logging.info("after training of {} epochs, {} has been saved.".format(epoch, ckpt_name))

                logging.info("# fall back to train mode")
                sess.run(train_init_op)
        summary_writer.close()
    logging.info("Training Done")


def prepro_data(config):
    if os.path.exists(config.prepro_path + 'train.en.bpe'):
        return None
    else:
        prepro = PrePro(config)
        prepro.run()
        logging.info("Pre-processing Done")


def main():
    config = Config()
    prepro_data(config)
    train_model(config)


if __name__ == '__main__':
    main()