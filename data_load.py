# -*- coding: utf-8 -*-
#/usr/bin/python3
'''
Note.
if safe, entities on the source side have the prefix 1, and the target side 2, for convenience.
For example, fpath1, fpath2 means source file path and target file path, respectively.
'''
import tensorflow as tf
from utils import calc_num_batches


class GetData(object):
    def __init__(self, config):
        self.prepro_path = config.prepro_path
        self.data_path = config.data_path
        self.maxlen_en = config.maxlen_en
        self.maxlen_cn = config.maxlen_cn

    def load_data(self, mode):
        '''Loads source and target data and filters out too lengthy samples.
        sents_en: list of source sents
        sents_cn: list of target sents
        '''
        sents_en, sents_cn, raw_list = [], [], []
        if mode == 'train':
            with open(self.prepro_path + 'train.en.bpe', 'r') as f_en, \
                    open(self.prepro_path + 'train.cn.bpe', 'r') as f_cn:
                for sent_en, sent_cn in zip(f_en, f_cn):
                    if len(sent_en.split()) + 1 > self.maxlen_en: continue
                    if len(sent_cn.split()) + 1 > self.maxlen_cn: continue
                    sents_en.append(sent_en.strip())
                    sents_cn.append(sent_cn.strip())
        elif mode == 'valid':
            with open(self.prepro_path + 'valid.en.bpe', 'r') as f_en, \
                    open(self.prepro_path + 'valid.cn.bpe', 'r') as f_cn, \
                    open(self.data_path + 'prepro/valid.cn', 'r') as f_valid:
                for sent_en, sent_cn, raw_cn in zip(f_en, f_cn, f_valid):
                    if len(sent_en.split()) + 1 > self.maxlen_en: continue
                    if len(sent_cn.split()) + 1 > self.maxlen_cn: continue
                    sents_en.append(sent_en.strip())
                    sents_cn.append(sent_cn.strip())
                    raw_list.append(raw_cn.strip())
            with open(self.data_path + 'prepro/filtered_valid.cn', 'w') as fout:
                fout.write("\n".join(raw_list) + "\n")
            with open(self.data_path + 'prepro/filtered_valid.en', 'w') as fout:
                fout.write("\n".join(sents_en) + "\n")
        else:
            print('can not load data %s' % mode)
            quit()
        return sents_en, sents_cn

    @staticmethod
    def load_vocab(prepro_path, mode='en'):
        '''Loads vocabulary file and returns idx<->token maps
        Note that these are reserved
        0: <pad>, 1: <unk>, 2: <s>, 3: </s>

        Returns
        two dictionaries.
        '''
        if mode == 'en':
            en_vocab = [line.split()[0] for line in open(prepro_path + 'en_bpe.vocab', 'r').read().splitlines()]
            en_token2idx = {token: idx for idx, token in enumerate(en_vocab)}
            en_idx2token = {idx: token for idx, token in enumerate(en_vocab)}
            return en_token2idx, en_idx2token
        elif mode == 'cn':
            cn_vocab = [line.split()[0] for line in open(prepro_path + 'cn_bpe.vocab', 'r').read().splitlines()]
            cn_token2idx = {token: idx for idx, token in enumerate(cn_vocab)}
            cn_idx2token = {idx: token for idx, token in enumerate(cn_vocab)}
            return cn_token2idx, cn_idx2token
        else:
            print('vocabulary only exist for english and chinese')
            quit()

    def encode(self, inp, _type, _dict):
        '''Converts string to number. Used for `generator_fn`.
        inp: 1d byte array.
        type: "x" (source side) or "y" (target side)
        dict: token2idx dictionary

        Returns
        list of numbers
        '''
        inp_str = inp.decode("utf-8")
        if _type == "x":
            # en sentence
            tokens = inp_str.split() + ["</s>"]
        else:
            # cn sentence
            tokens = ["<s>"] + inp_str.split() + ["</s>"]

        x = [_dict.get(t, _dict["<unk>"]) for t in tokens]
        return x

    def generator_fn(self, sents_en, sents_cn):
        '''Generates training / evaluation data
        sents_en: list of source sents
        sents_cn: list of target sents

        yields
        xs: tuple of
            x: list of source token ids in a sent
            x_seqlen: int. sequence length of x
            one_en_sent: str. raw source (=input) sentence
        labels: tuple of
            decoder_input: decoder_input: list of encoded decoder inputs
            y: list of target token ids in a sent
            y_seqlen: int. sequence length of y
            one_cn_sent: str. target sentence
        '''
        en_token2idx, _ = self.load_vocab(self.prepro_path, 'en')
        cn_token2idx, _ = self.load_vocab(self.prepro_path, 'cn')
        for one_en_sent, one_cn_sent in zip(sents_en, sents_cn):
            x = self.encode(one_en_sent, "x", en_token2idx)
            y = self.encode(one_cn_sent, "y", cn_token2idx)
            decoder_input, y = y[:-1], y[1:]

            x_seqlen, y_seqlen = len(x), len(y)
            yield (x, x_seqlen, one_en_sent), (decoder_input, y, y_seqlen, one_cn_sent)

    def input_fn(self, sents_en, sents_cn, batch_size, shuffle=False):
        '''Batchify data
        sents_en: list of source sents
        sents_cn: list of target sents
        batch_size: scalar
        shuffle: boolean

        Returns
        batch_x: tuple of
            x: int32 tensor. (N, T1)
            x_seqlens: int32 tensor. (N,)
            sents1: str tensor. (N,)
        batch_y: tuple of
            decoder_input: int32 tensor. (N, T2)
            y: int32 tensor. (N, T2)
            y_seqlen: int32 tensor. (N, )
            sents2: str tensor. (N,)
        '''
        shapes = (([None], (), ()),
                  ([None], [None], (), ()))
        types = ((tf.int32, tf.int32, tf.string),
                 (tf.int32, tf.int32, tf.int32, tf.string))
        paddings = ((0, 0, ''),
                    (0, 0, 0, ''))

        dataset = tf.data.Dataset.from_generator(
            self.generator_fn,
            output_shapes=shapes,
            output_types=types,
            args=(sents_en, sents_cn))  # <- arguments for generator_fn. converted to np string arrays

        if shuffle:  # for training
            dataset = dataset.shuffle(128 * batch_size)

        dataset = dataset.repeat()  # iterate forever
        dataset = dataset.padded_batch(batch_size, shapes, paddings).prefetch(1)

        return dataset

    def get_batch(self, batch_size, shuffle=False, mode='train'):
        '''Gets training / evaluation mini-batches
        batch_size: scalar
        shuffle: boolean

        Returns
        num_batches: number of mini-batches
        num_samples
        '''
        sents_en, sents_cn = self.load_data(mode)
        batches = self.input_fn(sents_en, sents_cn, batch_size, shuffle=shuffle)
        num_batches = calc_num_batches(len(sents_en), batch_size)
        return batches, num_batches, len(sents_en)