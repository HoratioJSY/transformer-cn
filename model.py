# -*- coding: utf-8 -*-
# /usr/bin/python3

import tensorflow as tf

from data_load import GetData
from modules import get_token_embeddings, feed_forward, positional_encoding, multihead_attention, label_smoothing, noam_scheme
from utils import convert_idx_to_token_tensor
from tqdm import tqdm
import logging

logging.basicConfig(level=logging.INFO)


class Transformer:
    '''
    xs: tuple of
        x: int32 tensor. (N, T1)
        x_seqlens: int32 tensor. (N,)
        sents1: str tensor. (N,)
    ys: tuple of
        decoder_input: int32 tensor. (N, T2)
        y: int32 tensor. (N, T2)
        y_seqlen: int32 tensor. (N, )
        sents2: str tensor. (N,)
    training: boolean.
    '''
    def __init__(self, config):
        self.config = config
        self.cn_token2idx, self.cn_idx2token = GetData.load_vocab(config.prepro_path, 'cn')
        self.en_embeddings = get_token_embeddings(config.en_vocab_size, config.d_model, zero_pad=True, mode='en')
        self.cn_embeddings = get_token_embeddings(config.cn_vocab_size, config.d_model, zero_pad=True, mode='cn')

    def encoder(self, xs, training=True):
        '''
        Returns
        memory: encoder outputs. (N, T1, d_model)
        '''
        with tf.variable_scope("encoder", reuse=tf.AUTO_REUSE):
            x, seq_lens, sents_en = xs

            # src_masks, (N, T1)
            src_masks = tf.math.equal(x, 0)

            # embedding, (N, T1, d_model)
            enc = tf.nn.embedding_lookup(self.en_embeddings, x)
            # re-scale
            enc *= self.config.d_model**0.5

            enc += positional_encoding(enc, self.config.maxlen_en)
            enc = tf.layers.dropout(enc, self.config.dropout_rate, training=training)

            # Encoder Blocks
            for i in range(self.config.num_blocks):
                with tf.variable_scope("num_blocks_{}".format(i), reuse=tf.AUTO_REUSE):
                    # self-attention
                    enc = multihead_attention(queries=enc,
                                              keys=enc,
                                              values=enc,
                                              key_masks=src_masks,
                                              num_heads=self.config.num_heads,
                                              dropout_rate=self.config.dropout_rate,
                                              training=training,
                                              causality=False)
                    # feed forward
                    enc = feed_forward(enc, num_units=[self.config.d_feedforward, self.config.d_model])
        memory = enc
        return memory, sents_en, src_masks

    def decoder(self, ys, memory, src_masks, training=True):
        '''
        memory: encoder outputs. (N, T1, d_model)
        src_masks: (N, T1)

        Returns
        logits: (N, T2, V). float32.
        y_hat: (N, T2). int32
        y: (N, T2). int32
        sents2: (N,). string.
        '''
        with tf.variable_scope("decoder", reuse=tf.AUTO_REUSE):
            decoder_inputs, y, seqlens, sents_cn = ys

            # tgt_masks
            tgt_masks = tf.math.equal(decoder_inputs, 0)  # (N, T2)

            # embedding
            dec = tf.nn.embedding_lookup(self.cn_embeddings, decoder_inputs)  # (N, T2, d_model)
            dec *= self.config.d_model ** 0.5  # scale

            dec += positional_encoding(dec, self.config.maxlen_cn)
            dec = tf.layers.dropout(dec, self.config.dropout_rate, training=training)

            # Blocks
            for i in range(self.config.num_blocks):
                with tf.variable_scope("num_blocks_{}".format(i), reuse=tf.AUTO_REUSE):
                    # Masked self-attention (Note that causality is True at this time)
                    dec = multihead_attention(queries=dec,
                                              keys=dec,
                                              values=dec,
                                              key_masks=tgt_masks,
                                              num_heads=self.config.num_heads,
                                              dropout_rate=self.config.dropout_rate,
                                              training=training,
                                              causality=True,
                                              scope="self_attention")

                    # Vanilla attention
                    dec = multihead_attention(queries=dec,
                                              keys=memory,
                                              values=memory,
                                              key_masks=src_masks,
                                              num_heads=self.config.num_heads,
                                              dropout_rate=self.config.dropout_rate,
                                              training=training,
                                              causality=False,
                                              scope="vanilla_attention")
                    # Feed Forward
                    dec = feed_forward(dec, num_units=[self.config.d_feedforward, self.config.d_model])

        # Final linear projection (embedding weights are shared), (d_model, vocab_size)
        weights = tf.transpose(self.cn_embeddings)
        # (N, T2, vocab_size)
        logits = tf.einsum('ntd,dk->ntk', dec, weights)
        y_hat = tf.to_int32(tf.argmax(logits, axis=-1))

        return logits, y_hat, y, sents_cn

    def train(self, xs, ys):
        '''
        Returns
        loss: scalar.
        train_op: training operation
        global_step: scalar.
        summaries: training summary node
        '''
        # forward
        memory, sents_en, src_masks = self.encoder(xs)
        logits, preds, y, sents_cn = self.decoder(ys, memory, src_masks)

        # train scheme, target vocab size
        y_ = label_smoothing(tf.one_hot(y, depth=self.config.cn_vocab_size))
        ce = tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=y_)
        nonpadding = tf.to_float(tf.not_equal(y, self.cn_token2idx["<pad>"]))  # 0: <pad>
        loss = tf.reduce_sum(ce * nonpadding) / (tf.reduce_sum(nonpadding) + 1e-7)

        global_step = tf.train.get_or_create_global_step()
        lr = noam_scheme(self.config.lr, global_step, self.config.warmup_steps)
        optimizer = tf.train.AdamOptimizer(lr)
        train_op = optimizer.minimize(loss, global_step=global_step)

        tf.summary.scalar('lr', lr)
        tf.summary.scalar("loss", loss)
        tf.summary.scalar("global_step", global_step)

        summaries = tf.summary.merge_all()

        return loss, train_op, global_step, summaries

    def eval(self, xs, ys):
        '''Predicts autoregressively
        At inference, input ys is ignored.
        Returns
        y_hat: (N, T2)
        '''
        decoder_inputs, y, y_seqlen, sents_cn = ys

        decoder_inputs = tf.ones((tf.shape(xs[0])[0], 1), tf.int32) * self.cn_token2idx["<s>"]
        ys = (decoder_inputs, y, y_seqlen, sents_cn)

        memory, sents_en, src_masks = self.encoder(xs, False)

        logging.info("Inference graph is being built. Please be patient.")
        for _ in tqdm(range(self.config.maxlen_cn)):
            logits, y_hat, y, sents_en = self.decoder(ys, memory, src_masks, False)
            if tf.reduce_sum(y_hat, 1) == self.cn_token2idx["<pad>"]: break

            _decoder_inputs = tf.concat((decoder_inputs, y_hat), 1)
            ys = (_decoder_inputs, y, y_seqlen, sents_en)

        # monitor a random sample
        n = tf.random_uniform((), 0, tf.shape(y_hat)[0]-1, tf.int32)
        sent_en = sents_en[n]
        pred = convert_idx_to_token_tensor(y_hat[n], self.cn_idx2token)
        sent_cn = sents_cn[n]

        tf.summary.text("sent_en", sent_en)
        tf.summary.text("pred", pred)
        tf.summary.text("sent_cn", sent_cn)
        summaries = tf.summary.merge_all()

        return y_hat, summaries

